package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class JavaMac extends AppCompatActivity {

    private SecretKey keyS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_mac);
        /*KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance("HmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        keyS = keyGenerator.generateKey();*/

    }


    public void convertMac(View view) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
        EditText editText = (EditText)findViewById(R.id.editText);
        TextView textViewMac = (TextView)findViewById(R.id.textViewMac);
        TextView textViewMessageEncrypt = (TextView)findViewById(R.id.textViewMessageEncrypt);
        //TextView textViewMessageDecrypt = (TextView)findViewById(R.id.textViewMessageDecrypt);


        Spinner mySpinner = (Spinner) findViewById(R.id.spinner);
        String algo = mySpinner.getSelectedItem().toString();
        KeyGenerator keyGenerator = KeyGenerator.getInstance(algo);
        SecretKey key = keyGenerator.generateKey();

        Mac mac = Mac.getInstance(key.getAlgorithm());
        mac.init(key);

        byte[] data = editText.getText().toString().getBytes("UTF-8");
        byte[] macBytes = mac.doFinal(data);

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < macBytes.length; i++)
            hexString.append(Integer.toHexString(0xFF & macBytes[i]));
        textViewMac.setText(hexString.toString());

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        //Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        //cipherDecrypt.init(Cipher.DECRYPT_MODE, key);
        byte[] cipherText = cipher.doFinal(data);
        hexString = new StringBuffer();
        for (int i = 0; i < cipherText.length; i++)
            hexString.append(Integer.toHexString(0xFF & cipherText[i]));
        textViewMessageEncrypt.setText(hexString.toString());

        //byte[] textDecrypt = cipherDecrypt.doFinal(cipherText);
        /*hexString = new StringBuffer();
        for (int i = 0; i < textDecrypt.length; i++)
            hexString.append(Integer.toHexString(0xFF & textDecrypt[i]));
        textViewMessageDecrypt.setText(hexString.toString());*/

    }

    public void javaMacTest() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
        /*int a;
        Mac mac = Mac.getInstance("HmacSHA256");
        byte[] keyBite = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        String algorithm = "RawBytes";
        SecretKeySpec keySpec = new SecretKeySpec(keyBite, algorithm);
        mac.init(keySpec);

        byte[] data = "CedricCrypto".getBytes("UTF-8");
        byte[] data1 = "Cedric".getBytes("UTF-8");
        byte[] data2 = "Crypto".getBytes("UTF-8");
        byte[] macBytes = mac.doFinal(data);
        mac.update(data1);
        byte[] macBytes2 = mac.doFinal(data2);
        // IS MAC = Hash + Encrypt
        byte[] dataDigest = "abcdef".getBytes("UTF-8");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] encrypt = cipher.doFinal(data);
        byte[] digest = messageDigest.digest(encrypt);
        a = 35;*/


        Mac mac = Mac.getInstance("HmacSHA256");
        byte[] keyBite = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        String algorithm = "RawBytes";
        SecretKeySpec keySpec = new SecretKeySpec(keyBite, algorithm);
        mac.init(keySpec);

        byte[] data = "CedricCrypto".getBytes("UTF-8");
        byte[] macBytes = mac.doFinal(data);

        // IS MAC = Hash + Encrypt
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] digest = messageDigest.digest(data);
        byte[] encrypt = cipher.doFinal(digest);
        int a = 56;

    }
}