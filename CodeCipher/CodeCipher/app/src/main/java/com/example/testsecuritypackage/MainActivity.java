package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


//Inspirée du site web : http://tutorials.jenkov.com/java-cryptography/signature.html
//Malheuresement une bonne partie du code a du évoluée depuis ce tutorial ce qui entraîne un certain nombre d'erreurs.

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void goMessageDigest(View view){
        Intent intent = new Intent(this, JavaMessageDigest.class);
        startActivity(intent);
    }


    public void goSignature(View view){
        Intent intent = new Intent(this, JavaSignature.class);
        startActivity(intent);
    }

    public void goMac(View view){
        Intent intent = new Intent(this, JavaMac.class);
        startActivity(intent);
    }

    public void goAbout(View view){
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }


}