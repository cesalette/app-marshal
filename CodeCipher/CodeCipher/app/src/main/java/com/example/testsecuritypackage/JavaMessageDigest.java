package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class JavaMessageDigest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_message_digest);
    }

    public void exempleJavaMessageDigest() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] data = "abcdef".getBytes("UTF-8");
        byte[] data1 = "aBc".getBytes("UTF-8");
        byte[] data2 = "de".getBytes("UTF-8");
        byte[] data3 = "f".getBytes("UTF-8");

        // To create a Java MessageDigest instance you call the static getInstance(). The text
        // parameter passed is the name of the concrete message digest algorithm to use.
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] digest = messageDigest.digest(data);

        // If you have multiple blocks of data to include in the same message digest, call the update()
        // method and finish off with a call to digest()
        messageDigest.update(data1);
        messageDigest.update(data2);
        byte[] digest2 = messageDigest.digest(data3);
        // digest & digest2 are equal.
    }


    public void convertMessageDigest(View view) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        EditText editText = (EditText) findViewById(R.id.editText);
        TextView textView = (TextView) findViewById(R.id.textViewMessage);
        if (editText.getText().toString().equals("")) {
            editText.setHintTextColor(Color.RED);
        } else {
            byte[] data = editText.getText().toString().getBytes("UTF-8");
            Spinner mySpinner = (Spinner) findViewById(R.id.spinner);
            String algo = mySpinner.getSelectedItem().toString();

            MessageDigest messageDigest = MessageDigest.getInstance(algo);
            byte[] result = messageDigest.digest(data);

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < result.length; i++)
                hexString.append(Integer.toHexString(0xFF & result[i]));
            textView.setText(hexString.toString());
        }
    }

}