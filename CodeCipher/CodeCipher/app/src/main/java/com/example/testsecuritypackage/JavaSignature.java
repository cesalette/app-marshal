package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;

public class JavaSignature extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_signature);
        try {
            javaSignatureTest();
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException | SignatureException e) {
            e.printStackTrace();
        }
    }


    public void convertMessageSignature(View view) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, SignatureException {
        EditText editText = (EditText)findViewById(R.id.editText);
        EditText editTextSign = (EditText)findViewById(R.id.editViewMessage);

        Signature signature = Signature.getInstance("SHA256WithDSA");
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        signature.initSign(keyPair.getPrivate(), secureRandom);
        byte[] data = editText.getText().toString().getBytes("UTF-8");
        signature.update(data);
        byte[] result = signature.sign();

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < result.length; i++)
            hexString.append(Integer.toHexString(0xFF & result[i]));
        editTextSign.setText(hexString.toString());






        TextView textView = (TextView)findViewById(R.id.textViewResult);
        Signature signatureVerify = Signature.getInstance("SHA256WithDSA");
        signatureVerify.initVerify(keyPair.getPublic());
        signatureVerify.update(data);
        boolean check = signatureVerify.verify(result);
        if (check){
            textView.setText("True signature");
        } else {
            textView.setText("Wrong signature");
        }
    }

    public void checkSignature(View view) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, SignatureException {
        EditText editText = (EditText)findViewById(R.id.editViewMessage);
        EditText editTextMessage = (EditText)findViewById(R.id.editText);
        TextView textView = (TextView)findViewById(R.id.textViewResult);

        Signature signature = Signature.getInstance("SHA256WithDSA");
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        signature.initVerify(keyPair.getPublic());

        byte[] data = editText.getText().toString().getBytes("UTF-8");
        signature.update(editTextMessage.getText().toString().getBytes("UTF-8"));
        boolean check = signature.verify(data);
        if (check){
            textView.setText("True signature");
        } else {
            textView.setText("Wrong signature");
        }
    }

    public void javaSignatureTest() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, SignatureException {
        Signature signature = Signature.getInstance("SHA256WithDSA");
        SecureRandom secureRandom = new SecureRandom();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        KeyPair keyFalsePair = keyPairGenerator.generateKeyPair();
        signature.initSign(keyPair.getPrivate(), secureRandom);

        byte[] data = "CedricCrypto".getBytes("UTF-8");
        signature.update(data);
        byte[] dataSigned = signature.sign();

        byte[] data1 = "Cedric".getBytes("UTF-8");
        byte[] data2 = "Crypto".getBytes("UTF-8");
        signature.update(data1);
        signature.update(data2);
        byte[] dataSigned2 = signature.sign();


        Signature verifySignature = Signature.getInstance("SHA256WithDSA");
        verifySignature.initVerify(keyPair.getPublic());
        verifySignature.update("CedricCrypto".getBytes("UTF-8"));
        boolean check = verifySignature.verify(dataSigned2);



        int a = 56;

    }
}