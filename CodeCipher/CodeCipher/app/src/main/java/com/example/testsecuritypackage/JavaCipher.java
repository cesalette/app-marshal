package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Signature;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

public class JavaCipher extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_cipher);
        javaCryptographie();
        try {
            javaCipher();
        } catch (
                NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (
                NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (
                InvalidKeyException e) {
            e.printStackTrace();
        } catch (
                BadPaddingException e) {
            e.printStackTrace();
        } catch (
                UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (
                IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (
                ShortBufferException e) {
            e.printStackTrace();
        }
    }


    public void javaCipher() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException, ShortBufferException {

        // Creates a Cipher instance using the encryption algorithm called AES
        Cipher cipherEncrypt = Cipher.getInstance("AES");
        Cipher cipherDecrypt = Cipher.getInstance("AES");
        // Creates a Cipher instance using Cipher Block Chaining (CBC)
        Cipher cipher1 = Cipher.getInstance("AES/CBC/PKCS5Padding");

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        SecureRandom secureRandom = new SecureRandom();
        int keyBitSize = 256;
        keyGenerator.init(keyBitSize, secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();

        cipherEncrypt.init(Cipher.ENCRYPT_MODE, secretKey);
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKey);

        /*
        byte[] plainText = "CedricCedric".getBytes("UTF-8");
        byte[] cipherText = cipher.doFinal(plainText);
        byte[] decryptText = cipher2.doFinal(cipherText);
        */


        byte[] data1 = "Paul".getBytes("UTF-8");
        byte[] data2 = "Mathieu".getBytes("UTF-8");
        byte[] data3 = "Hadrien".getBytes("UTF-8");
        byte[] cypherText1 = cipherEncrypt.update(data1);
        byte[] cypherText2 = cipherEncrypt.update(data2);
        byte[] cypherText3 = cipherEncrypt.doFinal(data3);


        /*byte[] clearText1 = cipher2.update(cypherText1);
        byte[] clearText2 = cipher2.update(cypherText2);*/

        /*int offset = 0;
        int length = 4;
        byte[] clearText3 = cipher2.doFinal(cypherText3, 1, 4);*/


        int a = 3;
    }


    public void javaCryptographie() {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            byte[] keyBytes = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
            String algorithm = "RawBytes";
            SecretKeySpec key = new SecretKeySpec(keyBytes, algorithm);

            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] plainText = "abcdefghijk".getBytes("UTF-8");
            byte[] cipherText = cipher.doFinal(plainText);


            // GENERATING A KEY || Symetric encryption/decryption
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = new SecureRandom();
            int keyBitSize = 256;
            keyGenerator.init(keyBitSize, secureRandom);
            SecretKey secretKey = keyGenerator.generateKey();
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);


            //GENERATING A KEY PAIR || Asymetric encryption/decryption
            SecureRandom secureRandom1 = new SecureRandom();
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            KeyPair keyPair = keyPairGenerator.generateKeyPair();


            //MESSAGE DIGEST
            // Use the SHA-256 cryptographic hash algorithm internally to calculte message digests
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] data1 = "1023456789".getBytes("UTF-8");
            byte[] data2 = "bacdefghijklmnopqrstuvwxyz".getBytes("UTF-8");

            messageDigest.update(data1);
            messageDigest.update(data2);

            byte[] digest = messageDigest.digest();


            //MAC (Message Authentication Code) : Similar to a message digest but uses an additional key
            Mac mac = Mac.getInstance("HmacSHA256");
            byte[] keyBytes1 = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
            String algorithm1 = "RawBytes";
            SecretKeySpec key1 = new SecretKeySpec(keyBytes1, algorithm1);

            mac.init(key1);

            byte[] data = "abcdefgggggggg".getBytes("UTF-8");
            byte[] macBytes = mac.doFinal(data);

            //SIGNATURE
            // The signature is created by creating a message digest (hash) from the data
            // and encrypting that message digest with the private key of the person that is to sign the data

            SecureRandom secureRandom3 = new SecureRandom();

            Signature signature = Signature.getInstance("SHA256WithRSA");
            signature.initSign(keyPair.getPrivate(), secureRandom);

            byte[] data21 = "abcdefghijklmnop".getBytes("UTF-8");
            signature.update(data21);

            byte[] digitalSignature = signature.sign();

            //Verifying Signature.
            Signature signatureVerify = Signature.getInstance("SHA256WithRSA");
            signatureVerify.initVerify(keyPair.getPublic());
            signatureVerify.update(data21);

            boolean check = signatureVerify.verify(digitalSignature);

            //Full Signature and Verification Example
            SecureRandom secureRandomF = new SecureRandom();
            KeyPairGenerator keyPairGeneratorF = KeyPairGenerator.getInstance("DSA");
            KeyPair keyPairF = keyPairGeneratorF.generateKeyPair();

            Signature signatureF = Signature.getInstance("SHA256WithDSA");
            signatureF.initSign(keyPairF.getPrivate(), secureRandomF);

            byte[] dataF = "abcdefghijklmnopqrstuvwxyz".getBytes("UTF-8");
            signatureF.update(dataF);
            byte[] digitalSignatureF = signatureF.sign();

            Signature signatureF2 = Signature.getInstance("SHA256WithDSA");
            signatureF2.initVerify(keyPairF.getPublic());
            signatureF2.update(dataF);

            boolean verified = signatureF2.verify(digitalSignatureF);

            Log.i("myTag", "VERIFIED = " + verified);
            int a = 25;


        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            throw new IllegalStateException("Could not retrieve AES cipher", e);
        }
    }

}