package com.example.testsecuritypackage;

import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


public class MyProvider {
    public static void main(String[] args){
        Security.addProvider(new BouncyCastleProvider());
    }
}
