package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class JavaKeyStore extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_key_store);
        try {
            javaKeyStoreTest();
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableEntryException e) {
            e.printStackTrace();
        }
    }

    public void javaKeyStoreTest() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] keyStorePassword = "123abc".toCharArray();

        // Load the KeyStore of the keystore.ks file
        /*try(InputStream keyStoreData = new FileInputStream("keystore.ks")){
            keyStore.load(keyStoreData, keyStorePassword);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | CertificateException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/

        // If you don't wanna load any data in the keyStore
        keyStore.load(null, keyStorePassword);
        char[] keyPassword = "789xyz".toCharArray();
        KeyStore.ProtectionParameter entryPassword = new KeyStore.PasswordProtection(keyPassword);
        KeyStore.Entry keyEntry = keyStore.getEntry("keyAlias", entryPassword);
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("keyAlias", entryPassword);

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        SecretKey secretKey = keyGenerator.generateKey();
        KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(secretKey);
        keyStore.setEntry("keyAliasSecretKey", secretKeyEntry, entryPassword);

        KeyStore.Entry keyEntry1 = keyStore.getEntry("keyAliasSecretKey", entryPassword);
        char[] myKeyStorePassword = "abcdef".toCharArray();
        /*try (FileOutputStream keyStoreoutPutStream = new FileOutputStream("data/keyStore.ks")){
            keyStore.store(keyStoreoutPutStream, myKeyStorePassword);
        }*/
        int a = 22;
    }
}