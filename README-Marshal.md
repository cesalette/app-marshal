#TEST MARSHAL ON 2 ANDROID EMULATOR.

FOR WINDOWS



#Get the project

clone the project in your workplace
git clone https://gitlab.isima.fr/palafour/code-marshal.git


#Android studio

Download Android Studio at https://developer.android.com/studio
In the home page, go to configure/SDK Manager/SDK tools
Make sure that the following package are installed and up to date : 
		-Android SDK Build-Tools 30-rc4
		-Android Emulator
		-Android SDK Platform-Tools
		-Android SDK Tools
		-Google Play Instant Development SDK
		-Google Play services
		-Intel x86 Emulator Accelerator (HAXM installer)

Select "Open an existing Android Studio project" and select the file "build.gradle" that you can 
find in workplace/code-marshal/app/build.gralde.

Open the AVD Manager and create at least 2 Virtual Device. (select API level : 29, at the time I wrote this).
After the 2 virtual devices are created click on "Run on Multiple Device".













