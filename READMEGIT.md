#Develop Marshal and pull in patches from Signal

#Clone Signal
git clone https://github.com/signalapp/Signal-Android.git

#Rename the remote pointed on Signal
git remote rename origin upstream

#Add your own repository remote as origin (here app-marshal)
git remote add origin https://gitlab.isima.fr/cesalette/app-marshal.git



#To pull in patches from Signal
git pull upstream master

#To push on app-marshal
git push origin master